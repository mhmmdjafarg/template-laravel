<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProfileIdToQuestion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('question', function (Blueprint $table) {
            $table->unsignedBigInteger('profile_id');
            $table->unsignedBigInteger('answer_correct_id');
            $table->foreign('profile_id')->references('id')->on('profile');
            $table->foreign('answer_correct_id')->references('id')->on('answer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('question', function (Blueprint $table) {
            $table->dropForeign(['answer_correct_id']);
            $table->dropForeign(['profile_id']);
            $table->dropColumn(['answer_correct_id']);
            $table->dropColumn(['profile_id']);
        });
    }
}
