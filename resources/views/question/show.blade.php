@extends('adminlte.master')

@section('content')
    <div class="card-header mt-3 ml-3">
        <h2 class="card-title">{{$question->title}}</h2>
    </div>
    <div class="card-body ml-3">
        <p class="card-text">{{$question->body}}</p>
    </div>
@endsection