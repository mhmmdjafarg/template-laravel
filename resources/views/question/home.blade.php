@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Question Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if(session('success'))
                   <div class="alert alert-success">
                        {{session('success')}}
                    </div> 
                @endif
              <a class="btn btn-primary mb-3" href="pertanyaan/create">Ask new question</a>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Title</th>
                    <th>Body</th>
                    <th style="width: 40px">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse($questions as $key => $question)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$question->title}}</td>
                            <td>{{$question->body}}</td>
                            <td style="display: flex">
                                <a href="/pertanyaan/{{$question->id}}" class="btn btn-info btn-sm">Detail</a>
                                <a href="/pertanyaan/{{$question->id}}/edit" class="btn btn-secondary btn-sm">Edit</a>
                                <form action="/pertanyaan/{{$question->id}}" method="POST">
                                  @csrf
                                  @method('DELETE')
                                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" align="center">No question yet</td>
                        </tr>
                    {{-- @endempty --}}
                    @endforelse
                  {{-- <tr>
                    <td>1.</td>
                    <td>Update software</td>
                    <td>
                      <div class="progress progress-xs">
                        <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                      </div>
                    </td>
                    <td><span class="badge bg-danger">55%</span></td>
                  </tr> --}}
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
              <ul class="pagination pagination-sm m-0 float-right">
                <li class="page-item"><a class="page-link" href="#">«</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">»</a></li>
              </ul>
            </div>
          </div>
    </div>
@endsection